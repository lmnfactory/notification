<?php

namespace Lmn\Notification\Repository;

use Lmn\Core\Lib\Repository\AbstractEventEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Core\Lib\Database\Save\SaveService;
use Lmn\Notification\Database\Model\Notification;

class NotificationRepository extends AbstractEventEloquentRepository {

    private $saveService;

    public function __construct(CriteriaService $criteriaService, SaveService $saveService) {
        parent::__construct($criteriaService);
        $this->saveService = $saveService;
    }

    public function getModel() {
        return Notification::class;
    }

    public function create($data) 
    {
        $model = parent::create($data);

        return $this->criteria('core.id', ['id' => $model->id])
            ->get();
    }
}
