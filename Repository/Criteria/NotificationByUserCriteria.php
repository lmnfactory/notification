<?php

namespace Lmn\Notification\Repository\Criteria;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class NotificationByUserCriteria implements Criteria {

    private $userId;

    public function __construct() {

    }

    public function set($data) {
        $this->userId = $data['userId'];
    }

    public function apply(Builder $builder) {
        $builder->where('notification.user_id', '=', $this->userId);
    }
}
