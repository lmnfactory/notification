<?php

namespace Lmn\Notification\Repository\Criteria;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class NotificationDefaultCriteria implements Criteria {

    public function __construct() {

    }

    public function set($data) {
        
    }

    public function apply(Builder $builder) {
        $builder->orderBy('notification.created_at', 'DESC');
    }
}
