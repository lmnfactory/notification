<?php

namespace Lmn\Notification\Repository\Criteria;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class NotificationNotseenCriteria implements Criteria {

    public function __construct() {

    }

    public function set($data) {
        
    }

    public function apply(Builder $builder) {
        $builder->where('notification.seen', '=', false);
    }
}
