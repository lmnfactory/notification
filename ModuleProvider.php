<?php

namespace Lmn\Notification;

use Lmn\Core\Provider\LmnServiceProvider;
use Lmn\Core\Lib\Module\ModuleServiceProvider;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;

use Lmn\Account\Middleware\SigninRequiredMiddleware;

use Lmn\Notification\Lib\Notification\NotificationService;
use Lmn\Notification\Lib\NotificationDb\NotificationService as NotificationDbService;
use Lmn\Notification\Lib\Notification\PushServerHandler;
use Lmn\Notification\Repository\NotificationRepository;

use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Notification\Repository\Criteria\NotificationDefaultCriteria;
use Lmn\Notification\Repository\Criteria\NotificationByUserCriteria;
use Lmn\Notification\Repository\Criteria\NotificationNotseenCriteria;
use Lmn\Notification\Repository\Criteria\NotificationNewCriteria;

use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Notification\Database\Validation\SeenValidation;

class ModuleProvider implements ModuleServiceProvider{

    public function register(LmnServiceProvider $provider) {
        $app = $provider->getApp();
        $app->singleton(NotificationService::class, NotificationService::class);
        $app->singleton(NotificationDbService::class, NotificationDbService::class);

        $app->singleton(NotificationRepository::class, NotificationRepository::class);
    }

    public function boot(LmnServiceProvider $provider) {
        $app = $provider->getApp();

        /** @var CriteriaService $criteriaService */
        $criteriaService = $app->make(CriteriaService::class);
        $criteriaService->add('notification.default', NotificationDefaultCriteria::class);
        $criteriaService->add('notification.by.user', NotificationByUserCriteria::class);
        $criteriaService->add('notification.new', NotificationNewCriteria::class);
        $criteriaService->add('notification.notseen', NotificationNotseenCriteria::class);

        /** @var ValidationService $validationService */
        $validationService = \App::make(ValidationService::class);
        $validationService->add('notification.seen', SeenValidation::class);
    }

    public function event(LmnServiceProvider $provider) {

    }

    public function route(LmnServiceProvider $provider) {
        Route::group(['namespace' => 'Lmn\\Notification\\Controller'], function() {
            Route::post('api/notification/list','NotificationController@getList')->middleware(SigninRequiredMiddleware::class);
            Route::post('api/notification/seen','NotificationController@seen')->middleware(SigninRequiredMiddleware::class);
            Route::post('api/notification/seen_all','NotificationController@seenAll')->middleware(SigninRequiredMiddleware::class);
            Route::post('api/notification/clear_new','NotificationController@clearNew')->middleware(SigninRequiredMiddleware::class);
        });
    }

    public function registerCommands($provider){

    }
}
