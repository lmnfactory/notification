<?php

namespace Lmn\Notification\Lib\NotificationDb;

use Lmn\Notification\Lib\Notification\NotificationService as NService;

class NotificationService {

    private $nService;

    public function __construct(NService $nService) {
        $this->nService = $nService;
    }

    public function pushNotification($notification) {
        return $this->nService->build()
            ->route('notification')
            ->to($notification->user_id)
            ->body($notification->toArray());
    }
}
