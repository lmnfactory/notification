<?php

namespace Lmn\Notification\Lib\Notification;

class PushServerHandler implements NotificationHandler {

    private $socket;
    private $error;

    public function __construct($addr) {
        $this->error = false;
        $context = new \ZMQContext();
        $this->socket = $context->getSocket(\ZMQ::SOCKET_PUSH, 'push server');
        //TODO: resolve if you cannot connect ot server
        try {
            $this->socket->connect($addr);
        }
        catch (\Exception $ex) {
            $this->error = true;
        }
    }

    public function notify(NotificationMessage $message) {
        if ($this->error === false) {
            $this->socket->send(json_encode($message->toArray()));
        }
    }
}
