<?php

namespace Lmn\Notification\Lib\Notification;

class NotificationMessage {

    private $to;
    private $message;

    public function __construct($message = null, $to = null) {
        $this->message = $message;
        $this->to = $to;
    }

    public function setMessage($message) {
        $this->message = $message;
        return $this;
    }

    public function setTo($to) {
        $this->to = $to;
        return $this;
    }

    public function getMessage() {
        return $this->message;
    }

    public function getTo() {
        return $this->to;
    }

    public function toArray() {
        return [
            'to' => $this->getTo(),
            'message' => $this->getMessage()
        ];
    }
}
