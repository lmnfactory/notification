<?php

namespace Lmn\Notification\Lib\Notification;

class NotificationBuilder {

    private $toValue;
    private $titleValue;
    private $descriptionValue;
    private $bodyValue;
    private $routeValue;

    private $notificationHandler;

    public function __construct(NotificationHandler $notificationHandler) {
        $this->notificationHandler = $notificationHandler;
        $this->toValue = null;
        $this->titleValue = "";
        $this->descriptionValue = "";
        $this->bodyValue = [];
        $this->routeValue = "";
    }

    public function route($value) {
        $this->routeValue = $value;
        return $this;
    }

    public function body($value) {
        $this->bodyValue = $value;
        return $this;
    }

    public function to($value) {
        $this->toValue = $value;
        return $this;
    }

    public function title($value) {
        $this->titleValue = $value;
        return $this;
    }

    public function description($value) {
        $this->descriptionValue = $value;
        return $this;
    }

    public function send() {
        $this->notificationHandler->notify(new NotificationMessage([
            'route' => $this->routeValue,
            'title' => $this->titleValue,
            'description' => $this->descriptionValue,
            'body' => $this->bodyValue
        ], $this->toValue));
    }
}
