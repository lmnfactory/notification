<?php

namespace Lmn\Notification\Lib\Notification;

interface NotificationHandler {
    public function notify(NotificationMessage $message);
}
