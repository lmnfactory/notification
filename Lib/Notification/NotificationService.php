<?php

namespace Lmn\Notification\Lib\Notification;

class NotificationService {

    private $method;

    public function __construct() {
        $this->method = null;
    }

    public function setMethod(NotificationHandler $method) {
        $this->method = $method;
    }

    public function notify(NotificationMessage $message) {
        if ($this->method == null) {
            return ;
        }

        $this->method->notify($message);
    }

    public function build() {
        return new NotificationBuilder($this->method);
    }
}
