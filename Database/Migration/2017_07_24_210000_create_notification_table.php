<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification', function (Blueprint $table) {
            $table->increments('id');
            $table->char('source', 50);
            $table->integer('source_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->char('title', 255);
            $table->char('text', 255);
            $table->boolean('seen')->default(false);
            $table->boolean('new')->default(true);
            $table->text('options');
            $table->timestamps();

            $table->index(['user_id', 'seen', 'new', 'created_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification');
    }
}
