<?php

namespace Lmn\Notification\Database\Validation;

use Lmn\Core\Lib\Model\LaravelValidation;

class SeenValidation extends LaravelValidation {

    public function getRules($data) {
        return [
            'id' => 'required|exists:notification,id'
        ];
    }
}
