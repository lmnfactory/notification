<?php

namespace Lmn\Notification\Database\Model;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model {

    protected $table = 'notification';

    protected $fillable = ['source', 'source_id', 'user_id', 'title', 'text', 'seen', 'new', 'options'];

    protected $casts = [
        'options' => 'array',
    ];
}
