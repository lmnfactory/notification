<?php

namespace Lmn\Notification\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Lmn\Core\Lib\Response\ResponseService;
use Lmn\Core\Lib\Model\ValidationService;

use Lmn\Account\Lib\Auth\AuthService;
use Lmn\Account\Lib\Auth\CurrentUser;

use Lmn\Notification\Repository\NotificationRepository;

class NotificationController extends Controller {

    public function getList(Request $request, ResponseService $responseService, ValidationService $validationService, CurrentUser $currentUser, NotificationRepository $notificationRepo) {
        $data = $request->json()->all();

        $list = $notificationRepo->clear()
            ->criteria('notification.by.user', ['userId' => $currentUser->getId()])
            ->criteria('notification.default')
            ->criteria('core.limit', ['limit' => 10])
            ->all();

        $time = time();
        
        $response = $responseService->createMessage($list);
        $response->setOption([
            'totalItems' => count($list),
            'timestamp' => $time
        ]);

        return $responseService->send($response);
    }

    public function seenAll(Request $request, ResponseService $responseService, ValidationService $validationService, CurrentUser $currentUser, NotificationRepository $notificationRepo) {
        $data = $request->json()->all();

        $notificationRepo->clear()
            ->criteria('notification.by.user', ['userId' => $currentUser->getId()])
            ->criteria('notification.notseen')
            ->update(['seen' => true]);
        
        $response = $responseService->createMessage(true);

        return $responseService->send($response);
    }

    public function seen(Request $request, ResponseService $responseService, ValidationService $validationService, CurrentUser $currentUser, NotificationRepository $notificationRepo) {
        $data = $request->json()->all();

        if (!$validationService->systemValidate($data, 'notification.seen')) {
            return $responseService->use('validation.system');
        }

        $notificationRepo->clear()
            ->criteria('notification.by.user', ['userId' => $currentUser->getId()])
            ->criteria('core.id', ['id' => $data['id']])
            ->update(['seen' => true]);
        
        $response = $responseService->createMessage(true);

        return $responseService->send($response);
    }

    public function clearNew(Request $request, ResponseService $responseService, ValidationService $validationService, CurrentUser $currentUser, NotificationRepository $notificationRepo) {
        $data = $request->json()->all();

        $notificationRepo->clear()
            ->criteria('notification.by.user', ['userId' => $currentUser->getId()])
            ->criteria('notification.new')
            ->update(['new' => false]);
        
        $response = $responseService->createMessage(true);

        return $responseService->send($response);
    }
}
